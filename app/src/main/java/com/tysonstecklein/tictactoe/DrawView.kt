package com.tysonstecklein.tictactoe

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class DrawView : View {

    private val paint = Paint()
    private val fillBrush = Paint()
    private val selectedBrush = Paint()
    private val strokeWidth = 3
    private val rect = Rect()
    private var rowCount = 0
    private var columnCount = 0
    private lateinit var hitRegions: Array<Rect>

//    private val selectedRegionsMap = HashMap<Int, Rect>()
//    private var selectedRegionIndices = mutableListOf<Int>()
    private var selectedRegions = mutableListOf<Rect>()

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context, attrs, defStyleAttr, defStyleRes)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawTicTacToeGrid(canvas)
        if(hitRegions.isNotEmpty()) {
            drawHitRegions(canvas)
        }
        if(selectedRegions.isNotEmpty()) {
            //need to map an array of rects from the selectedRegions array and send that
//            drawRegionsWithColor(canvas, selectedRegions.toTypedArray(), selectedBrush)
        }
    }

    fun init(rect: Rect, rowCount: Int, columnCount: Int) {
        paint.color = ContextCompat.getColor(context, R.color.colorPrimary)
        paint.isAntiAlias = false
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = resources.displayMetrics.density * strokeWidth

        fillBrush.color = ContextCompat.getColor(context, R.color.fillColor)
        fillBrush.isAntiAlias = false
        fillBrush.strokeWidth = 1f
        fillBrush.style = Paint.Style.FILL

        selectedBrush.color = Color.argb(100, 255, 255, 255)
        selectedBrush.isAntiAlias = false
        selectedBrush.style = Paint.Style.FILL

        this.rowCount = rowCount
        this.columnCount = columnCount

        this.rect.left = rect.left
        this.rect.top = rect.top
        this.rect.right = rect.right
        this.rect.bottom = rect.bottom
    }

    private fun drawTicTacToeGrid(canvas: Canvas) {
        canvas.drawLine(rect.left.toFloat(), rect.top.toFloat(), rect.right.toFloat(), rect.top.toFloat(), paint)
        canvas.drawLine(rect.left.toFloat(), rect.bottom.toFloat(), rect.right.toFloat(), rect.bottom.toFloat(), paint)
        canvas.drawLine(rect.left.toFloat(), rect.top.toFloat(), rect.left.toFloat(), rect.bottom.toFloat(), paint)
        canvas.drawLine(rect.right.toFloat(), rect.top.toFloat(), rect.right.toFloat(), rect.bottom.toFloat(), paint)

        paint.color = ContextCompat.getColor(context, R.color.ticTacToeGrid)

        val columnWidth = rect.width() / columnCount
        val rowWidth = rect.height() / rowCount

        for(i in 1 until columnCount) {
            val x = (i * columnWidth).toFloat()
            canvas.drawLine(x, rect.top.toFloat(), x, rect.bottom.toFloat(), paint)
        }

        for(i in 1 until rowCount) {
            val y = ((i * rowWidth) + rect.top).toFloat()
            canvas.drawLine(rect.left.toFloat(), y, rect.right.toFloat(), y, paint)
        }
    }

    fun setHitRegions(squares: Array<Rect>) {
        this.hitRegions = squares
    }

    private fun drawHitRegions(canvas: Canvas) {
        for(i in 0 until hitRegions.size) {
            val rndInt = if (i % 2 == 0) 100 else 200
            fillBrush.setARGB(50, rndInt, rndInt, rndInt)
            val shrunkenRegion = Rect(
                    hitRegions[i].left + 10,
                    hitRegions[i].top + 10,
                    hitRegions[i].right - 10,
                    hitRegions[i].bottom - 10
                )
            canvas.drawRect(shrunkenRegion, fillBrush)
        }
    }

    private fun drawRegionsWithColor(canvas: Canvas, regions: Array<Rect>, paint: Paint) {
        for(i in 0 until regions.size) {

        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val rectIndex = getRectIndex(x, y)
                if(rectIndex > 0) {
                    selectedRegions.add(hitRegions[rectIndex])
                }
            }
            MotionEvent.ACTION_MOVE -> {}
            MotionEvent.ACTION_UP -> {}
            MotionEvent.ACTION_CANCEL -> {}
        }
        return true
    }

    private fun getRectIndex(x: Float, y: Float): Int {
        hitRegions.forEachIndexed { index, rect ->
            if(rect.contains(x.toInt(), y.toInt())) {
                return index
            }
        }
        return -1
    }
}