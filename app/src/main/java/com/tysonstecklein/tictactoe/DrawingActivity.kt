package com.tysonstecklein.tictactoe

import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout

class DrawingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawing)

        val layout = findViewById<ConstraintLayout>(R.id.main_layout)
        val drawView = DrawView(this)
        layout.addView(drawView)

        val difference = resources.displayMetrics.heightPixels - resources.displayMetrics.widthPixels
        val top = difference / 2
        val left = 0
        val bottom = resources.displayMetrics.heightPixels - (difference / 2)
        val right = resources.displayMetrics.widthPixels
        val rect = Rect(left, top, right, bottom)
        val rowCount = 3
        val columnCount = 4
        val squares = getTouchRegions(rect, rowCount, columnCount)
        drawView.init(rect, rowCount, columnCount)
        drawView.setHitRegions(squares)
    }

    private fun getTouchRegions(rect: Rect, rows: Int, columns: Int) : Array<Rect> {
        var rects = Array<Rect>(rows * columns) { Rect() }
        val columnWidth = (rect.right - rect.left) / columns
        val rowHeight = (rect.bottom - rect.top) / rows

        for(index in 0 until rects.size) {
            var row = (index / columns)
            var col = (index % columns)
            rects[index] = Rect(
                rect.left + (columnWidth * col),
                rect.top + (row * rowHeight),
                rect.left + (columnWidth * col) + columnWidth,
                rect.top + (row * rowHeight) + rowHeight
            )
        }
        return rects
    }
}
